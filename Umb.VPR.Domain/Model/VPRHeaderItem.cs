﻿using System;
using System.Collections.Generic;

namespace Umb.VPR.Domain.Model
{
    public class VPRItem
    {
        #region Ctor

        public VPRItem()
        {
            CreatedDate = DateTime.Now;
            LineItems = new HashSet<VPRLineItem>();
        }

        #endregion

        #region Properties

        public int Id { get; set; }
        public int MonthId { get; set; }
        public int MemberId { get; set; }
        public string MemberName { get; set; }
        public int VenueId { get; set; }
        public string VenueName { get; set; }
        public string PostalCode { get; set; }
        public DateTime CreatedDate { get; set; }

        #endregion

        #region Navigation

        public ICollection<VPRLineItem> LineItems { get; set; }

        #endregion
    }
}
