﻿using System;

namespace Umb.VPR.Domain.Model
{
    public class VPRLineItem
    {
        #region Ctor

        public VPRLineItem()
        {

        }

        #endregion

        #region Properties

        public int Id { get; set; }
        public int HeaderId { get; set; }
        public int EventType { get; set; }
        public int TotalNoOfEvents { get; set; }
        public int TotalNoOfDelegates { get; set; }
        public Decimal AverageDDR { get; set; }

        #endregion

        #region Navigation

        public virtual VPRItem Header { get; set; }

        #endregion
    }
}
