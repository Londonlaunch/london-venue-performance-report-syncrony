﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="packageinstaller.ascx.cs" Inherits="USNSourcePI.usn_source_package.packageinstaller" %>
<div class="umb-packages-view-wrapper">

    <!-- Package details -->
    <div class="flex items-center justify-center">
        <div class="umb-info-local-items">


            <div class="umb-package-icon">
                <img alt="uSkinned" src="https://uskinned.net/images/package-icon.png">
            </div>
            <div class="umb-package-info">
                <h4 class="umb-info-local-item"><strong>uSkinned Source Starter Kit Successfully Installed</strong></h4>
                <div class="umb-info-local-item">
                    <p><strong>The installer has setup some pages to get you started.</strong></p>
                    <p><strong>To understand all of the layouts and components available please refer to the online demo available on the uSkinned website.</strong></p>
                    <p><strong><a href="http://uskinned.net/demos/1418/" target="_blank" title="Link will open in a new window/tab">Source Demo</a></strong></p>
                    <p><strong>Go to the "Content" section to manage the installed website.</strong></p>
                </div>
            </div>
        </div>
    </div>
</div>


