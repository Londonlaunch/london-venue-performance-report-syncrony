﻿using System.Web.Mvc;
using System.Collections.Generic;
using Umbraco.Web.Mvc;
using Umb.VPR.ViewModels;
using System;
using System.Linq;
using Umbraco.Web;
using Umbraco.Web.PublishedContentModels;
using Umb.VPR.Data;
using Umb.VPR.Service;
using Umbraco.Core.Models;
using Umb.VPR.Domain.Model;

namespace Umb.VPR.Controllers
{
    public class VPRFormSurfaceController : SurfaceController
    {
        #region Properties

        protected IUnitOfWork DataContext { get; set; }
        protected IVPRService VPRService { get; set; }

        #endregion

        #region Ctor

        public VPRFormSurfaceController(IUnitOfWork dataContext, IVPRService vprService)
        {
            DataContext = dataContext;
            VPRService = vprService;
        }

        #endregion

        #region Controller Methods

        [ChildActionOnly]
        [ActionName("RenderVPRForm")]
        public ActionResult RenderVPRForm(int nodeId)
        {
            // Check if "Customer" member is logged in.
            if (Members.IsLoggedIn() && Members.GetCurrentMember().DocumentTypeAlias == "Customer")
            {
                // Fetch Member
                var member = Members.GetCurrentMember();
                // Fetch all available event types.
                var eventTypes = Umbraco.TypedContentSingleAtXPath("//VPREventTypeFolder").Children();
                return PartialView("VPRForms/_vprForm", new VPRFormViewModel
                {
                    CurrentNodeId = nodeId,
                    MemberId = Members.GetCurrentMemberId(),
                    MonthId = DateTime.Now.Month,
                    Venues = Umbraco.TypedContentAtXPath("//companyFolder")
                        .FirstOrDefault()
                        .Children()
                        .Where(
                            x => x.GetPropertyValue<int>("associatedMember") == member.Id
                        ).ToList(),
                    Items = new List<EventItemViewModel> {
                        new EventItemViewModel {Id = 1, EventTypes = eventTypes }
                    }
                });
            }
            else {
                return Content(String.Format("<div class=\"spc alert alert-success alert-dismissible fade in\" role=\"alert\"><div class=\"info\">{0}</div></div>", "Member Access is Required"));
            }            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HandleVPRForm(VPRFormViewModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["IsSuccessful"] = false;
                return JavaScript(string.Format("$('#VPRError_{0}').show(); $('#VPRError_{0}').html('{1}');", model.CurrentNodeId, "General Error"));
            }

            var currentNode = Umbraco.TypedContent(model.CurrentNodeId);
            var member = Umbraco.TypedMember(model.MemberId);
            var venue = Umbraco.TypedContent(model.VenueId);
            if (member != null && venue != null)
            {
                var vprItem = new VPRItem
                {
                    MonthId = model.MonthId,
                    MemberId = model.MemberId,
                    MemberName = member.Name,
                    VenueId = venue.Id,
                    VenueName = venue.GetPropertyValue<string>("companyName"),
                    PostalCode = venue.GetPropertyValue<string>("postalCode"),
                    CreatedDate = DateTime.Now
                };

                foreach (var item in model.Items)
                {
                    vprItem.LineItems.Add(new VPRLineItem
                    {
                        HeaderId = vprItem.Id,
                        EventType = item.EventType,
                        TotalNoOfEvents = item.TotalNoOfEvents,
                        TotalNoOfDelegates = item.TotalNoOfDelegates,
                        AverageDDR = item.AverageDDR
                    });
                }

                // Save the captured data to DB.
                VPRService.CreateItem(vprItem);
                
                // Update success flag (in a TempData key)
                TempData["IsSuccessful"] = true;
                TempData["Message"] = currentNode.GetProperty("submissionMessage").Value.ToString();

                // Redirect to current page. (ensures form fields are cleared)
                return RedirectToCurrentUmbracoPage();
            }
            else
            {
                // TODO: Error Venue or Member is null.
                TempData["IsSuccessful"] = false;
                return JavaScript(string.Format("$('#VPRError_{0}').show(); $('#VPRError_{0}').html('{1}');", model.CurrentNodeId, "General Error"));
            }
        }


        [HttpPost]
        public ActionResult AddRow(int index)
        {
            // Fetch all available event types.
            IEnumerable<IPublishedContent> eventTypes = Umbraco.TypedContentSingleAtXPath("//VPREventTypeFolder")
                .Children();

            // Create and initialize the new line item.
            var newRow = new EventItemViewModel { Id = index, EventTypes = eventTypes };
            ViewData.TemplateInfo.HtmlFieldPrefix = string.Format("Items[{0}]", index);
            return PartialView("~/Views/Shared/EditorTemplates/EventItemViewModel.cshtml", newRow);
        }

        #endregion
    }
}