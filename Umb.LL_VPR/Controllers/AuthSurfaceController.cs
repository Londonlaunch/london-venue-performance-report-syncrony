﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using Umb.VPR.ViewModels;

namespace Umb.VPR.Controllers
{
    public class AuthSurfaceController : SurfaceController
    {
        #region Controller Methods

        [HttpGet]
        [ActionName("RenderLogin")]
        public ActionResult RenderLogin()
        {
            var model = new LoginViewModel
            {
                ReturnUrl = (CurrentPage.Url != Request.Url.PathAndQuery) ?
                    Request.Url.PathAndQuery :
                    Umbraco.NiceUrl(Convert.ToInt32(CurrentPage.GetProperty("loginSuccessPage").Value))
            };

            return PartialView("VPRForms/_login", model);
        }

        [HttpPost]
        [ActionName("HandleLogin")]
        [ValidateAntiForgeryToken]
        public ActionResult HandleLogin(LoginViewModel model)
        {
            // Check if user is already logged in 
            if (Members.IsLoggedIn())
                Redirect(model.ReturnUrl);

            // Check if form data is valid.
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            // Attempt to Login
            if (Membership.ValidateUser(model.Username, model.Password))
            {
                FormsAuthentication.SetAuthCookie(model.Username, false);
                return Redirect(model.ReturnUrl);
            }
            else
            {
                // Login failed, 
                TempData.Add("LoginFailure", "Login failed, please try again.");
                return RedirectToCurrentUmbracoPage();
            }
        }
        
        [HttpGet]
        [ActionName("RenderRegister")]
        public ActionResult RenderRegister()
        {
            var model = new RegisterViewModel
            {
                ReturnUrl = (CurrentPage.Url != Request.Url.PathAndQuery) ?
                    Request.Url.PathAndQuery : 
                    Umbraco.NiceUrl(Convert.ToInt32(CurrentPage.GetProperty("registerSuccessPage").Value))
            };

            return PartialView("VPRForms/_register", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HandleRegister(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            // Model is valid, so let's try to create the member.
            var tempGUID = Guid.NewGuid().ToString();
            try
            {
                // Attempt to create and initialize the new member.
                IMember newMember = Services.MemberService.CreateMember(model.Email, model.Email, GetDisplayName(model.FirstName, model.LastName), "Customer");
                newMember.IsApproved = false;
                newMember.SetValue("firstName", model.FirstName);
                newMember.SetValue("lastName", model.LastName);
                newMember.SetValue("companyName", model.Company);
                newMember.SetValue("jobPosition", model.Position);
                newMember.SetValue("hasVerifiedEmail", false);
                newMember.SetValue("joinedDate", DateTime.Now);
                newMember.SetValue("emailVerifyGUID", tempGUID);

                // Persist new member to the database and set roles and password.
                Services.MemberService.Save(newMember);
                Services.MemberService.AssignRole(newMember.Id, "VPR");
                Services.MemberService.SavePassword(newMember, model.Password);
            }
            catch (Exception ex)
            {
                TempData["RegistrationFailed"] = ex.Message;
                //ModelState.AddModelError("", ex.Message);
                return CurrentUmbracoPage();
            }

            // Send Email verification email.
            //var emailFrom = GlobalSettings.GetPropertyValue<string>("websiteEmailAddress");
            //var mailHelper = new EmailHelper(SmtpHost, SmtpUsername, SmtpPassword);
            //mailHelper.SendVerifyEmail(model.Email, emailFrom, tempGUID);

            // Update success flag (in a TempData key)
            //TempData["IsSuccessful"] = true;
            return RedirectToCurrentUmbracoPage();
        }

        [HttpGet]
        [ActionName("RenderForgotPassword")]
        public ActionResult RenderForgotPassword()
        {
            return PartialView("VPRForms/_forgotPassword", new ForgotPasswordViewModel());
        }

        [HttpPost]
        [ActionName("HandleForgotPassword")]
        [ValidateAntiForgeryToken]
        public ActionResult HandleForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            // Find the member with the matching email address.
            var member = Services.MemberService.GetByEmail(model.EmailAddress);
            if (member != null)
            {
                // We found the member matching the email.
                // Initialize expiry timout (15 minutes).
                var expiryTime = DateTime.Now.AddMinutes(15);

                // Update member expiry guid.
                var resetGuid = expiryTime.ToString("ddMMyyyyHHmmssFFFF");
                member.SetValue("resetGUID", resetGuid);
                Services.MemberService.Save(member);

                // Send the member an email with a Password Reset GUID in it.
                //var emailFrom = "bruce@syncrony.com";
                //var emailHelper = new EmailHelper();
                //emailHelper.SendResetPasswordEmail(member.Email, emailFrom, resetGuid);
            }
            else
            {
                ModelState.AddModelError("", $"We could not find a customer account matching {model.EmailAddress}");
                return CurrentUmbracoPage();
            }

            return RedirectToCurrentUmbracoPage();
        }

        [HttpGet]
        [ActionName("RenderResetPassword")]
        public ActionResult RenderResetPassword()
        {
            return PartialView("LLForms/_resetPassword", new ResetPasswordViewModel());
        }

        [HttpPost]
        [ActionName("HandleResetPassword")]
        [ValidateAntiForgeryToken]
        public ActionResult HandleResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
                return CurrentUmbracoPage();

            // Find the member with the matching email address.
            var member = Services.MemberService.GetByEmail(model.EmailAddress);
            if (member != null)
            {
                // Get the querystring GUID
                var resetQS = Request.QueryString["resetGUID"];
                if (!string.IsNullOrEmpty(resetQS))
                {
                    // See if the QS matches the value on the member property
                    if (member.GetValue<string>("resetGUID") == resetQS)
                    {
                        // Got a match, now check to see if the exiry window is valid.
                        DateTime currentTime = DateTime.Now;
                        DateTime expiryTime = DateTime.ParseExact(resetQS, "ddMMyyyyHHmmssFFFF", null);
                        if (currentTime.CompareTo(expiryTime) < 0)
                        {
                            // Not expired, so update user password and remove the reset guid.
                            member.RawPasswordValue = model.Password;
                            member.SetValue("resetGUID", string.Empty);
                            Services.MemberService.Save(member);
                            Redirect("/member-login");
                        }
                        else
                        {
                            //ERROR: Reset GUID has expired
                            ModelState.AddModelError("", "Password Reset GUID has expired");
                            return CurrentUmbracoPage();
                        }
                    }
                    else
                    {
                        // ERROR: QS does not match what is stored on member property
                        ModelState.AddModelError("", "Invalid Password Reset GUID");
                        return CurrentUmbracoPage();
                    }
                }
                else
                {
                    // ERROR: No QS present - Invalid resetGUID
                    ModelState.AddModelError("", "Invalid Password Reset GUID");
                    return CurrentUmbracoPage();
                }
            }

            return RedirectToCurrentUmbracoPage();
        }

        #endregion

        #region Helper Methods

        private string GetDisplayName(string firstName, string lastName)
        {
            return $"{firstName} {lastName}";
        }

        #endregion
    }
}