﻿using Umbraco.Core;

namespace Umb.VPR.Components
{
    public class UmbracoEventHandlers : ApplicationEventHandler
    {
        #region Overrides

        protected override void ApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            base.ApplicationInitialized(umbracoApplication, applicationContext);
            DependencyInjectionConfig.RegisterDependencies();
        }

        #endregion
    }
}