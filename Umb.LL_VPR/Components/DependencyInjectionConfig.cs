﻿using System.Web.Mvc;
using System.Web.Http;
using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Umbraco.Web;
using Umb.VPR.Service;
using Umb.VPR.Data;

namespace Umb.VPR.Components
{
    public class DependencyInjectionConfig
    {
        #region Static Methods

        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            // Register the MVC controllers.
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            // Register the Web API controllers.
            builder.RegisterApiControllers(typeof(UmbracoApplication).Assembly);
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.Register(c => new MapperService()).As<IMapperService>().SingleInstance();
            builder.RegisterType<VPRDbContext>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<VPRService>().As<IVPRService>().InstancePerLifetimeScope();

            // Build the container.
            ILifetimeScope container = builder.Build();

            // Set the depenedency resolver.
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        #endregion
    }
}