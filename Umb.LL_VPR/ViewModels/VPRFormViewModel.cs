﻿using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Umb.VPR.ViewModels
{
    public class VPRFormViewModel
    {
        #region Ctor

        public VPRFormViewModel()
        {
            Items = new List<EventItemViewModel>();
            Venues = new List<IPublishedContent>();
        }

        #endregion

        #region Properties

        [Required]
        public int CurrentNodeId { get; set; }
        [Required]
        public int MemberId { get; set; }
        [Display(Name = "Select Capture Month")]
        [Required(ErrorMessage = "You must select a capture month.")]
        public int MonthId { get; set; }
        [Display(Name = "Select Venue")]
        [Required(ErrorMessage = "You must select a venue.")]
        public int VenueId { get; set; }

        public virtual IList<EventItemViewModel> Items { get; set; }

        public virtual IEnumerable<SelectListItem> Months
        {
            get
            {
                return new List<SelectListItem> {
                    new SelectListItem { Text = "Choose", Value = "", Selected = true},
                    new SelectListItem { Text = "January", Value = "1"},
                    new SelectListItem { Text = "February", Value = "2"},
                    new SelectListItem { Text = "March", Value = "3"},
                    new SelectListItem { Text = "April", Value = "4"},
                    new SelectListItem { Text = "May", Value = "5"},
                    new SelectListItem { Text = "June", Value = "6"},
                    new SelectListItem { Text = "July", Value = "7"},
                    new SelectListItem { Text = "August", Value = "8"},
                    new SelectListItem { Text = "September", Value = "9"},
                    new SelectListItem { Text = "October", Value = "10"},
                    new SelectListItem { Text = "November", Value = "11"},
                    new SelectListItem { Text = "December", Value = "12"}
                };
            }
        }

        public virtual IEnumerable<IPublishedContent> Venues { get; set; }
        
        public virtual IEnumerable<SelectListItem> VenueItems
        {
            get
            {
                return Venues.Select(item => {
                    return new SelectListItem
                    {
                        Text = item.GetPropertyValue<string>("companyName"),
                        Value = item.Id.ToString()
                    };
                });
            }
        }
        
        #endregion
    }
}