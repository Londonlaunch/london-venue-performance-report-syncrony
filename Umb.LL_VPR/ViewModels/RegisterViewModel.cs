﻿using System.ComponentModel.DataAnnotations;

namespace Umb.VPR.ViewModels
{
    public class RegisterViewModel
    {
        #region Ctor

        public RegisterViewModel()
        {

        }

        #endregion

        #region Properties

        [Required(ErrorMessage = "Please enter your first name.")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Please enter a value between 2 - 50 characters long")]
        [Display(Name = "First Name", Order = 1)]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your last name")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Please enter a value between 2 - 50 characters long")]
        [Display(Name = "Last Name", Order = 2)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your company")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Please enter a value between 2 - 50 characters long")]
        [Display(Order = 3)]
        public string Company { get; set; }

        [Required(ErrorMessage = "Please enter your position")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Please enter a value between 2 - 50 characters long")]
        [Display(Order = 4)]
        public string Position { get; set; }

        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Please enter an email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        [DataType(DataType.EmailAddress)]
        [System.Web.Mvc.Remote("CheckUserExists", "AuthSurface")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        [StringLength(14, MinimumLength = 6, ErrorMessage = "Please enter a password between 6 - 14 characters long")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter you password confirmation")]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirm password doesn't match, Please try again !")]
        public string ConfirmPassword { get; set; }
        
        public string ReturnUrl { get; set; }

        #endregion
    }
}