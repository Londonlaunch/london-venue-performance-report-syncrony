﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Umb.VPR.ViewModels
{
    public class EventItemViewModel
    {
        #region Ctor

        public EventItemViewModel()
        {
            EventTypes = new List<IPublishedContent>();
        }

        #endregion

        #region Properties

        [Required]
        public int Id { get; set; }

        public bool IsDeleted { get; set; } = false;

        [Required]
        [Display(Name = "Event Type")]
        public int EventType { get; set; }
        [Required]
        [Display(Name = "Total No Of Events")]
        public int TotalNoOfEvents { get; set; }
        [Required]
        [Display(Name = "Total No Of Delegates")]
        public int TotalNoOfDelegates { get; set; }
        [Required]
        [Display(Name = "Average DDR")]
        public Decimal AverageDDR { get; set; }

        public virtual IEnumerable<IPublishedContent> EventTypes { get; set; }

        public virtual IEnumerable<SelectListItem> EventTypeItems
        {
            get
            {
                return EventTypes.Select(item => {
                    return new SelectListItem
                    {
                        Text = item.GetPropertyValue<string>("eventTypeName"),
                        Value = item.Id.ToString()
                    };
                });
            }
        }

        #endregion
    }
}