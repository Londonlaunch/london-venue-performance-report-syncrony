﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Umb.VPR.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [DisplayName("Email address")]
        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address.")]
        public string EmailAddress { get; set; }

        public int CurrentNodeID { get; set; }
    }
}