﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;
using USNStarterKit.USNBusinessLogic;

namespace USNStarterKit.USNBlog
{
    /// <summary>
    /// Summary description for USNDataService
    /// </summary>
    public class USNDataService
    {
        #region Singleton

        protected static volatile USNDataService m_Instance = new USNDataService();
        protected static object syncRoot = new Object();

        protected USNDataService() { }

        public static USNDataService Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    lock (syncRoot)
                    {
                        if (m_Instance == null)
                            m_Instance = new USNDataService();
                    }
                }

                return m_Instance;
            }
        }

        #endregion

        public IPublishedContent GetLanding(IPublishedContent node)
        {
            string cacheKey = "GetLanding_USNBlogLanding_" + node.Id;

            var cached = USNCacheHelper.GetFromRequestCache(cacheKey) as IPublishedContent;
            if (cached != null)
            {
                return cached;
            }

            var landing = node.AncestorOrSelf("USNBlogLandingPage");

            // cache the result
            USNCacheHelper.AddToRequestCache(cacheKey, landing);

            return landing;
        }
    }
}