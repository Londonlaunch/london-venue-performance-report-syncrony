﻿namespace Umb.VPR.Service
{
    public interface IMapperService
    {
        TDestination MapModel<TSource, TDestination>(TSource source);
        TDestination MapModel<TDestination>(object source);
        void Map<TSource, TDestination>(TSource source, TDestination destination);
    }
}
