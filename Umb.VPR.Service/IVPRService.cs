﻿using System.Collections.Generic;
using Umb.VPR.Domain.Model;

namespace Umb.VPR.Service
{
    public interface IVPRService
    {
        IList<VPRItem> GetAll();
        VPRItem Get(int headerId);
        void CreateItem(VPRItem item);
        IList<VPRLineItem> GetAllLineItemsById(int headerId);
    }
}
