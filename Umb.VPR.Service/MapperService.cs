﻿using AutoMapper;

namespace Umb.VPR.Service
{
    public class MapperService : IMapperService
    {
        #region IMapperService Implementation

        public TDestination MapModel<TSource, TDestination>(TSource source)
        {
            return Mapper.Map<TSource, TDestination>(source);
        }

        public void Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            Mapper.Map(source, destination);
        }

        public TDestination MapModel<TDestination>(object source)
        {
            return Mapper.Map<TDestination>(source);
        }

        #endregion
    }
}
