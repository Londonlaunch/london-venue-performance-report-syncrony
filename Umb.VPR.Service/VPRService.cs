﻿using System.Collections.Generic;
using System.Linq;
using Umb.VPR.Data;
using Umb.VPR.Domain.Model;

namespace Umb.VPR.Service
{
    public class VPRService : IVPRService
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;

        #endregion

        #region Ctor

        public VPRService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion

        #region Service Methods

        public void CreateItem(VPRItem item)
        {
            _unitOfWork.With<VPRItem>().Add(item);
            _unitOfWork.SaveChanges();
        }

        public IList<VPRItem> GetAll()
        {
            return _unitOfWork.With<VPRItem>().Select(x => x).ToList();
        }
        
        public VPRItem Get(int headerId)
        {
            return _unitOfWork.With<VPRItem>().Select(x => x).Where(x => x.Id == headerId).SingleOrDefault();
        }

        public IList<VPRLineItem> GetAllLineItemsById(int headerId)
        {
            return _unitOfWork.With<VPRLineItem>().Select(x => x).Where(x => x.HeaderId == headerId).ToList();
        }

        #endregion
    }
}
