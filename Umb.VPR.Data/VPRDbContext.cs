﻿using System.Data.Entity;

namespace Umb.VPR.Data
{
    public class VPRDbContext : DbContext, IUnitOfWork
    {
        #region Ctor

        public VPRDbContext() : base("Name=umbracoDbDSN")
        {
            Database.SetInitializer<VPRDbContext>(null);
        }

        #endregion

        #region Overrides

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(GetType().Assembly);
        }

        #endregion

        #region Public Methods

        public IDbSet<T> With<T>() where T : class
        {
            return Set<T>();
        }

        #endregion
    }
}
