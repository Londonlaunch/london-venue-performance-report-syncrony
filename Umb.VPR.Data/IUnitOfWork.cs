﻿using System;
using System.Data.Entity;

namespace Umb.VPR.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IDbSet<T> With<T>() where T : class;
        int SaveChanges();
    }
}
