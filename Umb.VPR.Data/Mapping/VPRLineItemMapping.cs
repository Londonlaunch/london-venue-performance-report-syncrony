﻿using System.Data.Entity.ModelConfiguration;
using Umb.VPR.Domain.Model;

namespace Umb.VPR.Data.Mapping
{
    public class VPRLineItemMapping : EntityTypeConfiguration<VPRLineItem>
    {
        #region Ctor

        public VPRLineItemMapping()
        {
            HasKey(t => t.Id);
            Property(t => t.HeaderId).IsRequired();
            Property(t => t.EventType).IsRequired();
            Property(t => t.TotalNoOfEvents).IsRequired();
            Property(t => t.TotalNoOfDelegates).IsRequired();
            Property(t => t.AverageDDR).IsRequired();
        }

        #endregion
    }
}
