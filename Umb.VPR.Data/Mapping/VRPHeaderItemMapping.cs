﻿using System.Data.Entity.ModelConfiguration;
using Umb.VPR.Domain.Model;

namespace Umb.VPR.Data.Mapping
{
    public class VRPHeaderItemMapping : EntityTypeConfiguration<VPRItem>
    {
        #region Ctor

        public VRPHeaderItemMapping()
        {
            HasKey(t => t.Id);
            Property(t => t.MonthId).IsRequired();
            Property(t => t.MemberId).IsRequired();
            Property(t => t.MemberName).HasMaxLength(150).IsRequired();
            Property(t => t.VenueId).IsRequired();
            Property(t => t.VenueName).HasMaxLength(150).IsRequired();
            Property(t => t.PostalCode).HasMaxLength(10).IsRequired();
            Property(t => t.CreatedDate).IsRequired();
        }

        #endregion
    }
}
