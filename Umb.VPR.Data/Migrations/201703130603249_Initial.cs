namespace Umb.VPR.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VPRHeaderItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MemberId = c.Int(nullable: false),
                        MemberName = c.String(nullable: false),
                        VenueId = c.Int(nullable: false),
                        VenueName = c.String(nullable: false),
                        PostalCode = c.String(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VPRLineItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        HeaderId = c.Int(nullable: false),
                        EventType = c.Int(nullable: false),
                        TotalNoOfEvents = c.Int(nullable: false),
                        TotalNoOfDelegates = c.Int(nullable: false),
                        AverageDDR = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.VPRLineItems");
            DropTable("dbo.VPRHeaderItems");
        }
    }
}
