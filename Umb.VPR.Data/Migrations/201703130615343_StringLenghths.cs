namespace Umb.VPR.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StringLenghths : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.VPRHeaderItems", "MemberName", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.VPRHeaderItems", "VenueName", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.VPRHeaderItems", "PostalCode", c => c.String(nullable: false, maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.VPRHeaderItems", "PostalCode", c => c.String(nullable: false));
            AlterColumn("dbo.VPRHeaderItems", "VenueName", c => c.String(nullable: false));
            AlterColumn("dbo.VPRHeaderItems", "MemberName", c => c.String(nullable: false));
        }
    }
}
