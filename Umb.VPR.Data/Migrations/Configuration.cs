namespace Umb.VPR.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Umb.VPR.Data.VPRDbContext>
    {
        #region Ctor

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        #endregion

        #region Ovverrides

        protected override void Seed(Umb.VPR.Data.VPRDbContext context)
        {
            
        }

        #endregion
    }
}
