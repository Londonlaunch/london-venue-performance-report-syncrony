namespace Umb.VPR.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Month : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VPRHeaderItems", "MonthId", c => c.Int(nullable: false));
            CreateIndex("dbo.VPRLineItems", "HeaderId");
            AddForeignKey("dbo.VPRLineItems", "HeaderId", "dbo.VPRHeaderItems", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VPRLineItems", "HeaderId", "dbo.VPRHeaderItems");
            DropIndex("dbo.VPRLineItems", new[] { "HeaderId" });
            DropColumn("dbo.VPRHeaderItems", "MonthId");
        }
    }
}
